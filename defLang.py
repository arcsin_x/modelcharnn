import string

def langDef(lang):
    print(lang)
    if lang == 'ru':
        lowercase = 'абвгдеёжзиклмнопрстуфъцчшщъыьэюя'
        uppercase = 'АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
        letters = lowercase + uppercase
        digits = '0123456789'
        punctuation = r"""!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"""
        whitespace = ' \t\n\r\v\f'
        printable = digits + letters + punctuation + whitespace
        n_characters = len(printable)
    else:
        printable = string.printable
        n_characters = len(printable)
    return printable, n_characters