# https://github.com/spro/char-rnn.pytorch
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unidecode
import random
import time
import math
import torch

# Reading and un-unicode-encoding data

#all_characters = string.printable
#n_characters = len(all_characters)

def read_file(filename, lang):
    if lang == 'ru':
        file = open(filename).read()
    else:
        file = unidecode.unidecode(open(filename).read())
    print('File: ', file[:100])
    return file, len(file)

# Turning a string into a tensor

def char_tensor(string, all_characters):
    tensor = torch.zeros(len(string)).long()
    for c in range(len(string)):
        try:
            tensor[c] = all_characters.index(string[c])
        except:
            continue
    #print('char_tensor: ', tensor)
    return tensor

# Readable time elapsed

def time_since(since):
    s = time.time() - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

